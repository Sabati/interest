module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        database: env('DATABASE_HOST', 'interest-db'),
        username: env('DATABASE_USER','interest-root'),
      },
      options: {
        useNullAsDefault: true,
        debug: true,
        autoMigration: true
      },
    },
  },
});
